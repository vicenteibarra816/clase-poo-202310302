<?php

interface OperacionesGenerales{
    public function redondeo($monto);
    public function CalcularIva($monto);
}
class PuntoDeVenta implements OperacionesGenerales{
    public function redondeo($monto){
        return round($monto);
    }
    public function CalcularIva($monto){
        return $iva = $monto*.16;
    }
}


?>